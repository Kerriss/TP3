// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

//cookie
var cookie = require('cookie');


// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});	
});

app.post("/login", function(req, res, next) {
	db.all('SELECT * FROM users WHERE ident=? AND password=?;', [req.body.identifiant, req.body.mot_de_passe], function(err, data){
	
	if(data.length){
		//l'utilisateurs à donné un login valide
							
		db.all('SELECT * FROM sessions WHERE ident=?;', [req.body.identifiant], function(err, data){
		var d = new Date();
		var token = Math.random() + req.body.identifiant +  d;

		if(data.length){
		//l'utilisateurs à déjà une session
			
			db.run('UPDATE INTO sessions VALUES(?,?);', [req.body.identifiant, token], function(err, data){
				
				res.json({status: true, token: token.toString()});
				var setCookie = cookie.serialize('key', token.toString());
				console.log(setCookie);

			});
		}
		else{
		// l'utilisateurs n'a pas de session
			db.all('INSERT INTO sessions VALUES(?,?);', [req.body.identifiant, token], function(err, data){
				res.json({status: true, token: token.toString()});

			});
		}
		
		});
	}
	else{
	//l'utilisateurs à donné un login invalide
		res.json({status: false});

	}

	});

});


// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
